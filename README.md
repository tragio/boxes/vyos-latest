# VyOS VM Builder

## Prerequsities

- Packer 1.5+ running on Linux
- VirtualBox running on Linux or Windows Host with WSL

## Overview

VyOS installation disc is used to bootstrap a VM.  On creation, it has one
ethernet adapter connected via internal LAN and the device is setup to
use a static IP of 192.168.1.1/24 (follows behavior of Ubiquiti ER).

This way, when used in a downstream environment, first action will be to
SSH in via 192.168.1.1/24 and update configuration based on final adapters.

## Build

`packer build template.json`

If there are any issues, it is probably timing related.  Make sure on VBox when vyos machine is running on details under "System" acceleration says "VT-x/AMD=V, Nested paging, PAE/NX, KVM Paravirtualization" otherwise it is emulating CPU in software.  If that is all on and still not building, turn the headless flag "false" in template and watch it build to see where it is binding up (following boot command as it runs).

## Provisioning

### Bring the environment up

Run `ip a` to ensure there are no interfaces which interfere with 192.168.1.0/24.  Then:

```
VBoxManage hostonlyif create
VBoxManage hostonlyif ipconfig --ip 192.168.1.2 --netmask 255.255.255.0 vboxnet0
vagrant up
```

** Note: if your Ubuntu machine is on a network leg of 192.168.1.0/24 this will obviously break.  Unfortunately, our
emulation of real networks requires that we use the same IP space as the target devices.  Recommend you NAT your dev VM
or router with a non-conflicting IP space.  **

### Destroying Box and Network

```
vagrant destroy
VBoxManage hostonlyif remove vboxnet0
```

### Adding an Internet adapter via NAT

Note: Not supported/tested.

```
VBoxManage natnetwork add --netname internet --network 10.255.255.0/24 --enable --dhcp on
VBoxManage natnetwork start --netname internet
```

## Troubleshooting

These templates use time delays which are potentially too short on a less
performant system.  If having trouble that is not apparent (stuck in boot),
turn "headless" off and see what's going on.

REMINDER: It is not unreasonable that you have an interface/network
internally that is 192.168.1.0/24... you will need to setup your env not
to conflict (NAT, change upstream addressing).

If you rebuild the box in packer and you are using the Vagrant file, it
caches it locally from Vagrant's perspective.  You will need to:

`vagrant box remove ./builds/vyos-latest.virtualbox.box`

then

`vagrant up`

Normally we would `vagrant box update` does not work because the local
copy is versionless.
